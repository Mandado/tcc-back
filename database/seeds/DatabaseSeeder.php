<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    Schema::enableForeignKeyConstraints();
    \DB::statement('truncate users cascade');
    \DB::statement('truncate patients cascade');
    $this->call(UsersTableSeeder::class);
    Schema::disableForeignKeyConstraints();
  }
}
