<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
  /**
  * Run the migrations.
  *
  * @return void
  */
  public function up()
  {
    Schema::create('patients', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name', 120);
      $table->integer('age');
      $table->string('cpf');
      $table->text('diagnostic');
      $table->char('sex', 2);
      $table->string('race', 40);
      $table->string('ocupation', 100);
      $table->string('street', 150);
      $table->integer('number');
      $table->string('neighborhood', 150);
      $table->string('city', 150);
      $table->string('state', 150);
      $table->string('telphone', 150);
      $table->string('responsible', 150)->nullable();;
      $table->string('academicResponsible', 150);
      $table->boolean('active');
      $table->integer('user_id')->unsigned();
      $table->foreign('user_id')->references('id')->on('users');
      $table->integer('staging_area_id')->unsigned();
      $table->foreign('staging_area_id')->references('id')->on('staging_areas');
      $table->timestamps();
    });
  }

  /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::drop('patients');
  }
}
