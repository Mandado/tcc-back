<?php

namespace App;

use \App\Patient;
use \App\User;
use \App\StagingArea;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WaitingList extends Model
{
    use SoftDeletes;
    
    protected $fillable = ['user_id', 'patient_id', 'staging_area_id'];
    protected $dates = ['deleted_at'];

    public function user() {
      return $this->belongsTo(User::class);
    }

    public function patient() {
      return $this->belongsTo(Patient::class);
    }

    public function stagingArea() {
      return $this->belongsTo(StagingArea::class);
    }
}
