<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
  protected $fillable = ['name','email','telphone','registration'];

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function sessionsAtended() {
    return $this->hasMany(SessionAtended::class);
  }

  public function evolutions() {
    return $this->hasMany(PatientEvolution::class);
  }

}
