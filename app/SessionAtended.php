<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SessionAtended extends Model
{
  protected $dates = ['month'];
  protected $fillable = ['month','monthTotal'];

  public function patient() {
    return $this->belongsTo(Patient::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function student(){
    return $this->belongsTo(Student::class);
  }
}
