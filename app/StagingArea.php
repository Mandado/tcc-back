<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StagingArea extends Model
{
  protected $fillable = ['name'];

  public function patients() {
    return $this->hasMany(Patient::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }
}
