<?php
namespace App\Repository;

use App\Student;

class StudentRepository extends BaseRepository
{
  protected $modelClass = Student::class;

  public function getOrderById($q = '') {
    $orderByIdQuery = $this->newQuery()->orderBy('id','desc');

    if($q){
      $orderByIdQuery->orWhere('name', 'like', "%{$q}%");
    }

    if($q){
      $orderByIdQuery->orWhere('email', 'like', "%{$q}%");
    }

    if($q){
      $orderByIdQuery->orWhere('registration', $q);
    }

    return $this->doQuery($orderByIdQuery);
  }

  public function reportSessionAtendeds($clause = []) {
    $query = $this->newQuery()->withCount([
      'sessionsAtended' => $this->setConditions($clause),
      'evolutions'      => $this->setConditions($clause, 'evolution_date')
    ])
    ->whereHas('sessionsAtended',$this->setConditions($clause))
    ->orWhereHas('evolutions', $this->setConditions($clause, 'evolution_date'))
    ->with(['sessionsAtended' => $this->sessionAtendedQuery()])
    ->get();

    return $query;
  }

  private function setWhereDate($query, $clause, $column) {
    $query->whereMonth($column, '>=', $clause['init'])
    ->whereMonth($column, '<=', $clause['final']);
  }

  private function setWhereStudent($query, $clause) {
    $query->where('student_id', $clause['student']);
  }

  private function setConditions($clause = [], $column = 'month') {
    return function($query) use ($clause, $column) {

      if($clause['init'] && $clause['final']) {
        $this->setWhereDate($query, $clause, $column);
      }

      if($clause['student']) {
        $this->setWhereStudent($query, $clause);
      }
    };
  }

  private function sessionAtendedQuery() {
    return function($query) {
      $query->selectRaw('student_id, sum("monthTotal") as aggregate')
            ->groupBy('student_id');
    };
  }

}
