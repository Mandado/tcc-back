<?php
namespace App\Repository;

use App\User;

class UserRepository extends BaseRepository
{
  protected $modelClass = User::class;

  public function getOrderById($q = '') {
    $orderByIdQuery = $this->newQuery()->orderBy('id','desc');

    if($q){
      $orderByIdQuery->where('name', 'like', "%{$q}%");
    }

    if($q){
      $orderByIdQuery->orWhere('email', 'like', "%{$q}%");
    }

    return $this->doQuery($orderByIdQuery);
  }

}
