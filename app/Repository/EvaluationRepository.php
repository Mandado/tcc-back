<?php
namespace App\Repository;

use App\Evaluation;

class EvaluationRepository extends BaseRepository
{
  protected $modelClass = Evaluation::class;

  public function findByStudent($id) {
    return $this->newQuery()->where('student_id', $id)->first();
  }

  public function findByStudentWithQuesits($id) {
    return $this->newQuery()->where('student_id', $id)->with('quesits')->first();
  }

  public function report($clause = []) {
    $query = $this->newQuery()
      ->with('quesits','student')
      ->whereYear('created_at', $clause['year']);

    if($clause['student']) {
      $query->where('student_id', $clause['student']);
    }

    return $query->get();
  }
}
