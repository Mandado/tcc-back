<?php
namespace App\Repository;

use App\WaitingList;

class WaitingListRepository extends BaseRepository
{
  protected $modelClass = WaitingList::class;

  public function getOrderByCreatedAt($q = '') {
    $orderByIdQuery = $this->newQuery()->orderBy('created_at',' asc');
    $orderByIdQuery->with('patient', 'stagingArea');
    return $this->doQuery($orderByIdQuery);
  }
}
