<?php
namespace App\Repository;

use App\StagingArea;

class StagingAreaRepository extends BaseRepository
{
  protected $modelClass = StagingArea::class;

  public function getOrderById($q = '') {
    $orderByIdQuery = $this->newQuery()->orderBy('id','desc');

    if($q){
      $orderByIdQuery->where('name', 'like', "%{$q}%");
    }

    return $this->doQuery($orderByIdQuery);
  }
}
