<?php
namespace App\Repository;

use App\Quesit;

class QuesitRepository extends BaseRepository
{
  protected $modelClass = Quesit::class;

  public function getOrderById($q = '') {
    $orderByIdQuery = $this->newQuery()->orderBy('id','desc');

    if($q){
      $orderByIdQuery->where('description', 'like', "%{$q}%");
    }

    return $this->doQuery($orderByIdQuery);
  }

  public function getActive() {
    $query = $this->newQuery()->where('active', true)
                  ->select('id', 'description', 'minNote', 'maxNote');

    return $this->doQuery($query, false, false);
  }
}
