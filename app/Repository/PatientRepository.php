<?php
namespace App\Repository;

use App\Patient;

class PatientRepository extends BaseRepository
{
  protected $modelClass = Patient::class;

  public function getOrderById($q = '') {
    $orderByIdQuery = $this->newQuery()
      ->with('stagingArea')
      ->orderBy('id','desc');

    if($q){
      $orderByIdQuery->whereHas('stagingArea', function($query) use ($q) {
        $query->where('name', 'ilike' ,"%{$q}%");
      });
    }

    if($q){
      $orderByIdQuery->orWhere('name', 'like', "%{$q}%");
    }

    if($q){
      $orderByIdQuery->orWhere('academicResponsible', 'like', "%{$q}%");
    }

    if($q){
      $orderByIdQuery->orWhere('cpf', $q);
    }

    return $this->doQuery($orderByIdQuery);
  }

  public function report($clause = []) {
    $query = $this->newQuery()
    ->whereYear('created_at', $clause['year'])
    ->get(['name','cpf','id']);

    return $query;
  }

  public function reportSessionAtendeds($clause = []) {
    $query = $this->newQuery()->withCount([
      'sessionsAtended' => $this->setConditions($clause),
      'evolutions'      => $this->setConditions($clause, 'evolution_date')
    ])
    ->whereHas('sessionsAtended',$this->setConditions($clause))
    ->orWhereHas('evolutions', $this->setConditions($clause, 'evolution_date'))
    ->with(['sessionsAtended' => $this->sessionAtendedQuery()])
    ->get();

    return $query;
  }

  private function setWhereDate($query, $clause, $column) {
    $query->whereMonth($column, '>=', $clause['init'])
    ->whereMonth($column, '<=', $clause['final']);
  }

  private function setWhereStudent($query, $clause) {
    $query->where('student_id', $clause['student']);
  }

  private function setConditions($clause = [], $column = 'month') {
    return function($query) use ($clause, $column) {

      if($clause['init'] && $clause['final']) {
        $this->setWhereDate($query, $clause, $column);
      }
    };
  }

  private function sessionAtendedQuery() {
    return function($query) {
      $query->selectRaw('patient_id, sum("monthTotal") as aggregate')
            ->groupBy('patient_id');
    };
  }

}
