<?php
namespace App\Repository;
use Artesaos\Warehouse\AbstractCrudRepository;

abstract class BaseRepository extends AbstractCrudRepository {

  public function findWith($id, $with, $fail = true) {
    if ($fail) {
        return $this->newQuery()->with($with)->findOrFail($id);
    }

    return $this->newQuery()->with($with)->find($id);
  }

}
