<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\StagingAreaRepository;
use App\Http\Requests;

class StagingAreasController extends Controller
{
  private $stagingAreaRepository;

  public function __construct(StagingAreaRepository $stagingAreaRepository){
    $this->stagingAreaRepository = $stagingAreaRepository;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $query = $request->get('q');

    return $this->stagingAreaRepository->getOrderById($query);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Requests\StagingAreaRequest $request)
  {
    try {
      $data = $request->only(['name']);
      $stagingArea = $this->stagingAreaRepository->factory($data);
      $stagingArea->user()->associate($request->user());
      $stagingArea->save();

      return response()->json([
        'success' => true,
        'message' => 'Área de estágio cadastrada com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar nova área de estágio.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao cadastrar área de estágio.'
      ]);
    }

  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    return $this->stagingAreaRepository->findByID($id);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Requests\StagingAreaRequest $request, $id)
  {
    try {
      $data = $request->only(['name']);
      $stagingArea = \App\StagingArea::find($id);
      $stagingArea->user()->associate($request->user());
      $this->stagingAreaRepository->update($stagingArea, $data);

      return response()->json([
        'success' => true,
        'message' => 'Área de estágio alterada com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar nova área de estágio.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao alterar dados da área de estágio.'
      ]);
    }

  }
}
