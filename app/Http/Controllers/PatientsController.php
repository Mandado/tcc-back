<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\PatientRepository;
use App\Http\Requests;

class PatientsController extends Controller
{
  private $patientsRepository;

  public function __construct(PatientRepository $patientsRepository){
    $this->patientsRepository = $patientsRepository;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $query = $request->get('q');

    return $this->patientsRepository->getOrderById($query);
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Requests\PatientRequest $request)
  {
    try {
      $data = $request->only([
        'name','age','cpf','sex','race',
        'diagnostic','ocupation','active',
        'street','number','neighborhood','city',
        'state','telphone','responsible',
        'academicResponsible'
      ]);

      $patient = $this->patientsRepository->factory($data);
      $patient->user()->associate($request->user());
      $patient->stagingArea()->associate($request->get('stagingArea'));
      $patient->save();

      return response()->json([
        'success' => true,
        'message' => 'Paciente cadastrado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar novo paciente.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao cadastrar o paciente.'
      ], 400);
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    return $this->patientsRepository->findWith($id, 'stagingArea');
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Requests\PatientRequest $request, $id)
  {
    try {
      $data = $request->only([
        'name','age','cpf','sex','race',
        'diagnostic','ocupation','active',
        'street','number','neighborhood','city',
        'state','telphone','responsible',
        'academicResponsible'
      ]);

      $patient = \App\Patient::find($id);
      $patient->user()->associate($request->user());
      $patient->stagingArea()->associate($request->get('stagingArea'));
      $this->patientsRepository->update($patient, $data);

      return response()->json([
        'success' => true,
        'message' => 'Paciente alterado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar nova área de estágio.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao alterar dados do paciente.'
      ], 400);
    }
  }

  public function importSessions($id, Request $request) {
    try {
      $date = (\Carbon\Carbon::now())->month($request->get('month'))->toDateTimeString();

      $sessionAtended = new \App\SessionAtended([
        'month' => $date,
        'monthTotal' => $request->get('monthTotal'),
      ]);

      $sessionAtended->user()->associate($request->user());
      $sessionAtended->student()->associate($request->get('student'));

      $patient = \App\Patient::find($id);
      $patient->sessionsAtended()->save($sessionAtended);

      return response()->json([
        'success' => true,
        'message' => 'Saldo anterior de atendimentos importado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar importar saldo anterior de atendimentos.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao tentar importar saldo anterior de atendimentos.'
      ], 400);
    }
  }

  public function evolution($id, Request $request) {
    try {
      $student = \App\Student::find($request->get('student'));
      $user = $request->user();

      $evolution = new \App\PatientEvolution([
        'description' => $request->get('evolution'),
        'evolution_date' => $request->get('date'),
      ]);

      $evolution->user()->associate($user);
      $evolution->student()->associate($student);

      $patient = \App\Patient::find($id);
      $patient->evolutions()->save($evolution);

      return response()->json([
        'success' => true,
        'message' => 'Evolução do paciente cadastrada com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar cadastrar evolução do paciente.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao tentar cadastrar evolução do paciente.'
      ], 400);
    }
  }

  public function evolutions($id)
  {
    try {
      $patient = \App\Patient::find($id);
      $evolutions = $patient->evolutions()->get(['id', 'description', 'evolution_date']);

      return response()->json($evolutions);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar obter a lista de evoluções.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao tentar obter a lista de evoluções.'
      ], 400);
    }
  }
  public function destroyEvolution($id)
  {
    try {
      $evolution = \App\PatientEvolution::find($id);
      $evolution->delete();

      return response()->json([
        'success' => true,
        'message' => 'Evolução do paciente excluida com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar excluir evolução do.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao tentar excluir evolução do paciente.'
      ], 400);
    }
  }
}
