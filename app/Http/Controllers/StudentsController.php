<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repository\StudentRepository;
use App\Repository\EvaluationRepository;

class StudentsController extends Controller
{
  private $studentRepository;
  private $evaluationRepository;

  public function __construct(StudentRepository $studentRepository, EvaluationRepository $evaluationRepository){
    $this->studentRepository = $studentRepository;
    $this->evaluationRepository = $evaluationRepository;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $query = $request->get('q');

    return $this->studentRepository->getOrderById($query);
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function store(Requests\StudentRequest $request)
  {
    try {
      $data = $request->only(['name','email','telphone','registration']);
      $stagingArea = $this->studentRepository->factory($data);
      $stagingArea->user()->associate($request->user());
      $stagingArea->save();

      return response()->json([
        'success' => true,
        'message' => 'Aluno cadastrado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar novo aluno.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao cadastrar aluno.'
      ]);
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    return $this->studentRepository->findByID($id);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try {
      $data = $request->only(['name','email','telphone','registration']);
      $student = \App\Student::find($id);
      $student->user()->associate($request->user());
      $this->studentRepository->update($student, $data);

      return response()->json([
        'success' => true,
        'message' => 'Aluno alterado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar alterar novo aluno.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao alterar aluno.'
      ]);
    }
  }

  public function getEvaluationByStudent($studentId) {
    $evaluation = $this->evaluationRepository->findByStudentWithQuesits($studentId);

    return response()->json(compact('evaluation'));
  }

  public function evaluation($id, Request $request)
  {
    try{

      $data = array_merge($request->only([
        'teacher','staging_supervisor','year','instituition',
        'class_school','supervisor_ajuster','observation'
      ]), ['student_id' =>  $id]);

      $evaluation = $this->buildEvaluation($id, $data);
      $evaluation->user()->associate($request->user());
      $evaluation->save();
      $evaluation->syncQuesits($request->get('quesits'));

      return response()->json([
        'success' => true,
        'message' => 'Avaliação do aluno cadastrada com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar salvar avaliação do aluno.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao salvar avaliação do aluno.'
      ]);
    }
  }

  private function buildEvaluation($studentId, $data){
    $evaluation = $this->evaluationRepository->findByStudent($studentId);

    if($studentId && $evaluation) {
      $evaluation->fill($data);
    }else {
      $evaluation = $this->evaluationRepository->factory($data);
    }

    return $evaluation;
  }
}
