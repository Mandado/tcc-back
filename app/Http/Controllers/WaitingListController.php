<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\WaitingListRepository;
use App\Http\Requests;

class WaitingListController extends Controller
{
  private $waitingListRepository;

  public function __construct(WaitingListRepository $waitingListRepository){
    $this->waitingListRepository = $waitingListRepository;
  }

  public function index() {
    return $this->waitingListRepository->getOrderByCreatedAt();
  }

  public function store(Requests\WaitingListRequest $request) {
    try {
      $patient = $request->get('patient');
      $stagingArea = $request->get('stagingArea');
      $waitingList = $this->waitingListRepository->factory(['patient_id' => $patient, 'staging_area_id' => $stagingArea]);
      $waitingList->user()->associate($request->user());
      $waitingList->save();

      return response()->json([
        'success' => true,
        'message' => 'Paciente cadastrado com sucesso na lista de espera.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar cadastrar paciente na lista de espera.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao cadastrar paciente na lista de espera.'
      ]);
    }
  }

  public function destroy($id, Request $request) {
    try {
      $waitingList = $this->waitingListRepository->findById($id);
      $waitingList->delete();

      return response()->json([
        'success' => true,
        'message' => 'Paciente removido com sucesso na lista de espera.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar remover paciente na lista de espera.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao remover paciente na lista de espera.'
      ]);
    }
  }
}
