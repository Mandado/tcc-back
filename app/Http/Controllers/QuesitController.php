<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\QuesitRepository;
use App\Http\Requests;

class QuesitController extends Controller
{
  private $quesitRepository;

  public function __construct(QuesitRepository $quesitRepository){
    $this->quesitRepository = $quesitRepository;
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index(Request $request)
  {
    $query = $request->get('q');

    return $this->quesitRepository->getOrderById($query);
  }
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function quesitActives()
  {
    return $this->quesitRepository->getActive();
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Requests\QuesitRequest $request)
  {
    try {
      $data   = $request->only(['description','minNote','maxNote','active']);
      $quesit = $this->quesitRepository->factory($data);
      $quesit->user()->associate($request->user());
      $quesit->save();

      return response()->json([
        'success' => true,
        'message' => 'Quesito cadastrado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar novo quesito.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao cadastrar quesito.'
      ], 400);
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    return $this->quesitRepository->findByID($id);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Requests\QuesitRequest $request, $id)
  {
    try {
      $data   = $request->only(['description','minNote','maxNote','active']);
      $quesit = \App\Quesit::find($id);
      $quesit->user()->associate($request->user());
      $this->quesitRepository->update($quesit, $data);

      return response()->json([
        'success' => true,
        'message' => 'Quesito alterada com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar nova área de estágio.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao alterar dados do quesito.'
      ], 400);
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    try{
      $waitingList = $this->quesitRepository->findById($id);
      $waitingList->delete();

      return response()->json([
        'success' => true,
        'message' => 'Quesito removido com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar remover quesito.', ['trace' => $e]);

      return response()->json([
        'success' => false,
        'message' => 'Erro ao remover quesito.'
      ], 400);
    }
  }
}
