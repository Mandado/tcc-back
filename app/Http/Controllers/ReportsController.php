<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\StudentRepository;
use App\Repository\PatientRepository;
use App\Repository\EvaluationRepository;
use App\Http\Requests;

class ReportsController extends Controller
{
  public function reportSessionAtendeds(Request $request) {
    $filters = $request->only('init', 'final');

    $query = app()->make(PatientRepository::class)->reportSessionAtendeds($filters);

    return response()->json($query);
  }
  
  public function reportSessionAtendedsByStudent(Request $request) {
    $filters = $request->only('init', 'final','student');

    $query = app()->make(StudentRepository::class)->reportSessionAtendeds($filters);

    return response()->json($query);
  }

  public function reportPatients(Request $request) {
    $filters = $request->only('year');

    $query = app()->make(PatientRepository::class)->report($filters);

    return response()->json($query);
  }

  public function reportEvaluationsStudent(Request $request) {
    $filters = $request->only('year','student');

    $query = app()->make(EvaluationRepository::class)->report($filters);

    return response()->json($query);
  }
}
