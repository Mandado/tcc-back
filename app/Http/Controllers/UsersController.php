<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Http\Requests;
use App\Repository\UserRepository;

class UsersController extends Controller
{
  private $userRepository;


  public function __construct(UserRepository $userRepository){
    $this->userRepository = $userRepository;
  }

  public function index(Request $request)
  {
    $query = $request->get('q');

    return $this->userRepository->getOrderById($query);
  }

  public function authenticate(Request $request) {
    // grab credentials from the request
    $credentials = $request->only('login', 'password');

    try {
      // attempt to verify the credentials and create a token for the user
      if (! $token = \JWTAuth::attempt($credentials)) {
        return response()->json(['msg' => 'Login ou senha inválidos.'], 401);
      }
    } catch (JWTException $e) {
      \Log::error('Erro na tentativa de login.', ['trace' => $e]);
      // something went wrong whilst attempting to encode the token
      return response()->json(['msg' => 'Não foi possível criar o token de acesso.'], 400);
    }

    // all good so return the token
    return response()->json(compact('token'));
  }

  public function store(Requests\UserRequest $request) {
    try {
      $data = $request->only(['name','email','login','password','telphone','registration']);
      $user = $this->userRepository->factory($data);
      $user->user()->associate($request->user());
      $user->save();

      return response()->json([
        'success' => true,
        'message' => 'Usuário cadastrado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar criar novo usuário.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao cadastrar usuário.'
      ]);
    }
  }

  public function show($id)
  {
    return $this->userRepository->findByID($id);
  }

  public function update(Request $request, $id)
  {
    try {
      $data = $request->only(['name','email','login','password','telphone','registration']);
      $student = \App\User::find($id);
      $this->userRepository->update($student, $data);

      return response()->json([
        'success' => true,
        'message' => 'Usuário alterado com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar alterar novo usuário.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao alterar aluno.'
      ]);
    }
  }

  public function destroy($id, Request $request) {
    try {
      $waitingList = $this->userRepository->findById($id);
      $waitingList->delete();

      return response()->json([
        'success' => true,
        'message' => 'Usuário removido com sucesso.'
      ]);
    } catch (Exception $e) {
      \Log::error('Erro ao tentar remover usuário.', ['trace' => $e]);

      return response()->json([
        'success' => true,
        'message' => 'Erro ao remover usuário.'
      ]);
    }
  }

  public function logout() {
    \JWTAuth::logout();
  }
}
