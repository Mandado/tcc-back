<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required',
          'age' => 'required|integer',
          'cpf' => 'required|unique:patients',
          'sex' => 'required',
          'race' => 'required',
          'diagnostic' => 'required',
          'ocupation' => 'required',
          'active' => 'required|boolean',
          'street' => 'required',
          'number' => 'required|integer',
          'neighborhood' => 'required',
          'city' => 'required',
          'state' => 'required',
          'telphone' => 'required|integer',
          'academicResponsible' => 'required',
        ];
    }
}
