<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuesitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'description' => 'required|unique:quesits,description',
          'maxNote' => 'required|numeric',
          'minNote' => 'required|numeric',
          'active' => 'required|boolean',
        ];
    }
}
