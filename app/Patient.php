<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
  protected $casts = [
    'active' => 'boolean',
    'age' => 'integer',
    'number' => 'integer',
    'telphone' => 'integer',
    'stagingArea' => 'integer',
  ];

  protected $fillable = [
    'name','age','cpf','sex','race',
    'diagnostic','ocupation','active',
    'street','number','neighborhood','city',
    'state','telphone','responsible',
    'academicResponsible'
  ];

  public function stagingArea(){
    return $this->belongsTo(StagingArea::class);
  }

  public function sessionsAtended() {
    return $this->hasMany(SessionAtended::class);
  }

  public function evolutions() {
    return $this->hasMany(PatientEvolution::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }
}
