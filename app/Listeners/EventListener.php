<?php

namespace App\Listeners;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  QueryExecuted  $event
     * @return void
     */
    public function handle(QueryExecuted $queryExecuted)
    {
        extract(get_object_vars($queryExecuted));

        $fullQuery = vsprintf(str_replace(array('%','?'), array('%%','%s'), $sql), $bindings);

        \Log::info("{$connectionName} | {$time}ms | {$fullQuery}". PHP_EOL);
    }
}
