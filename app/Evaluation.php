<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
  protected $fillable = ['teacher','staging_supervisor','year','instituition','class_school','supervisor_ajuster','observation', 'student_id'];

  public function quesits() {
    return $this->belongsToMany(Quesit::class, 'evaluation_quesits')
                ->withPivot('note')
                ->withTrashed()
                ->withTimestamps();
  }

  public function student(){
    return $this->belongsTo(Student::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function syncQuesits(array $quesits = []) {
    return $this->quesits()->sync($this->formatQuesits($quesits));
  }

  private function formatQuesits(array $quesits = []) {
    $quesitsCollection = [];

    foreach ($quesits as $quesit => $note) {
      $quesitsCollection[$quesit] = ['note' => $note];
    }

    return $quesitsCollection;
  }
}
