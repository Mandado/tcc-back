<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Quesit extends Model
{
  use SoftDeletes;

  protected $fillable = ['description','minNote','maxNote','active'];
  protected $dates = ['deleted_at'];
  protected $casts = [
    'minNote' => 'float',
    'maxNote' => 'float',
  ];

  public function user(){
    return $this->belongsTo(User::class);
  }

}
