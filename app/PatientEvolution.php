<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientEvolution extends Model
{
  protected $dates = ['evolution_date'];
  protected $fillable = ['description','evolution_date'];

  public function patients() {
    return $this->belongsTo(Patient::class);
  }

  public function user(){
    return $this->belongsTo(User::class);
  }

  public function student(){
    return $this->belongsTo(Student::class);
  }

  public function setEvolutionDateAttribute($value) {
    $this->attributes['evolution_date'] = \Carbon\Carbon::createFromFormat('d/m/Y', $value);
  }
}
