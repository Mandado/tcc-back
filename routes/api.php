<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/users/authenticated', function (Request $request) {
    $user = $request->user();

    return [
      'id' => $user->id,
      'name' => $user->name,
    ];
})->middleware('jwt.auth');

Route::post('/users/authenticate', 'UsersController@authenticate');
Route::post('/users/logout', 'UsersController@logout');
Route::group(['middleware' => 'jwt.auth'], function() {
  Route::get('/quesits/actives', 'QuesitController@quesitActives');
  Route::get('/patients/{id}/evolutions', 'PatientsController@evolutions');
  Route::get('/reports/sessions-atended', 'ReportsController@reportSessionAtendeds');
  Route::get('/reports/sessions-atended-by-student', 'ReportsController@reportSessionAtendedsByStudent');
  Route::get('/reports/patients', 'ReportsController@reportPatients');
  Route::get('/reports/evaluation-students', 'ReportsController@reportEvaluationsStudent');
  Route::get('/students/{id}/evaluation', 'StudentsController@getEvaluationByStudent');
  Route::post('/students/{id}/evaluation', 'StudentsController@evaluation');
  Route::post('/patients/{id}/import-sessions', 'PatientsController@importSessions');
  Route::post('/patients/{id}/evolution', 'PatientsController@evolution');
  Route::delete('/patients/{id}/evolution', 'PatientsController@destroyEvolution');

  Route::resource('waiting-list', 'WaitingListController', ['only' => ['index','store','destroy']]);
  Route::resource('staging-areas', 'StagingAreasController');
  Route::resource('patients', 'PatientsController');
  Route::resource('students', 'StudentsController');
  Route::resource('quesits', 'QuesitController');
  Route::resource('users', 'UsersController');

});
